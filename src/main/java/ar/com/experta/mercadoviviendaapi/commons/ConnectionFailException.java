package ar.com.experta.mercadoviviendaapi.commons;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.REQUEST_TIMEOUT)
public class ConnectionFailException extends RuntimeException{
    private static final long serialVersionUID=1l;

    public ConnectionFailException(Exception exception){
        super(exception);
    }

    public ConnectionFailException(String msg){
        super (msg);
    }

}
