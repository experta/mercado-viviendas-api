package ar.com.experta.mercadoviviendaapi.commons;


import ar.com.experta.mercadoviviendaapi.commons.exceptions.BadGatewayException;
import ar.com.experta.mercadoviviendaapi.commons.exceptions.BadRequestException;
import ar.com.experta.mercadoviviendaapi.commons.exceptions.NotFoundException;
import ar.com.experta.mercadoviviendaapi.commons.exceptions.NotImplementedException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Arrays;
import java.util.stream.Collectors;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.BAD_REQUEST.value(),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        ex.getMessage(),
                        Arrays.asList(ex.getMessage())),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request);
    }

    // this code is to  @Valid when wrong request arrive
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(  MethodArgumentNotValidException ex,
                                                                    HttpHeaders headers,
                                                                    HttpStatus status,
                                                                    WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.BAD_REQUEST.value(),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        ex.getMessage(),
                ex.getBindingResult()
                        .getFieldErrors()
                        .stream()
                        .map(x -> x.getDefaultMessage())
                        .collect(Collectors.toList())
                        ),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND,
                request);
    }


    @ExceptionHandler(value = {NotImplementedException.class})
    protected ResponseEntity<Object> handleNotImplementedException(NotImplementedException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.NOT_IMPLEMENTED.value(),
                        HttpStatus.NOT_IMPLEMENTED.getReasonPhrase(),
                        null,
                        null),
                new HttpHeaders(),
                HttpStatus.NOT_IMPLEMENTED,
                request);
    }

    @ExceptionHandler(value = {NotFoundException.class})
    protected ResponseEntity<Object> handleNotFoundException(NotFoundException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.NOT_FOUND.value(),
                        HttpStatus.NOT_FOUND.getReasonPhrase(),
                        ex.getLocalizedMessage(),
                        null),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND,
                request);
    }

    @ExceptionHandler(value = {BadRequestException.class})
    protected ResponseEntity<Object> handleBadRequestException(BadRequestException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.BAD_REQUEST.value(),
                        HttpStatus.BAD_REQUEST.getReasonPhrase(),
                        ex.getLocalizedMessage(),
                        ex.getMessages()),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND,
                request);
    }

    @ExceptionHandler(value = {BadGatewayException.class})
    protected ResponseEntity<Object> handleBadGatewayException(BadGatewayException ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.BAD_GATEWAY.value(),
                        HttpStatus.BAD_GATEWAY.getReasonPhrase(),
                        ex.getLocalizedMessage(),
                        null),
                new HttpHeaders(),
                HttpStatus.BAD_GATEWAY,
                request);
    }

    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        return handleExceptionInternal(ex, new ErrorMessage(
                        HttpStatus.INTERNAL_SERVER_ERROR.value(),
                        HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                        ex.getLocalizedMessage(),
                        null
                        ),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR,
                request);
    }


}