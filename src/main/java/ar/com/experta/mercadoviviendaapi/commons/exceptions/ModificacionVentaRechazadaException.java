package ar.com.experta.mercadoviviendaapi.commons.exceptions;

public class ModificacionVentaRechazadaException extends BadRequestException {

    public enum MotivoRechazo {
        VENTA_EMITIDA("No se puede modificar el cliente ya que la venta ya se encuentra emitida"),
        COTIZACION_VENCIDA_NO_VALIDA ("No se puede modificar la cotizacion a estado VENCIDA") ;

        private String message;

        MotivoRechazo(String message) {
            this.message = message;
        }

        public String message() {
            return message;
        }
    }

    public ModificacionVentaRechazadaException(MotivoRechazo motivoRechazo) {
        super(motivoRechazo.message);
    }
}
