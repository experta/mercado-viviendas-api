package ar.com.experta.mercadoviviendaapi.commons.exceptions;

public class PlanNotFoundException extends NotFoundException{

    public PlanNotFoundException() {
        super("El plan buscado no existe");
    }
}
