package ar.com.experta.mercadoviviendaapi.commons.exceptions;

public class CotizacionNotFoundException extends NotFoundException{

    public CotizacionNotFoundException() {
        super("La cotizacion buscada no existe");
    }
}
