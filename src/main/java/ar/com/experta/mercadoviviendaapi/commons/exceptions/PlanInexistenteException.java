package ar.com.experta.mercadoviviendaapi.commons.exceptions;

public class PlanInexistenteException extends NotFoundException {

    public PlanInexistenteException(String idPlan) {
        super("No se ha encontrado el plan " + idPlan);
    }
}
