package ar.com.experta.mercadoviviendaapi.commons.exceptions;

public abstract class NotFoundException extends RuntimeException {

    protected NotFoundException(String message) {
        super(message);
    }

}
