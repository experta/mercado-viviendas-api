package ar.com.experta.mercadoviviendaapi.commons.exceptions;

public class BadGatewayException extends RuntimeException {

    public BadGatewayException(String message) {
        super(message);
    }
}
