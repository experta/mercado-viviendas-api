package ar.com.experta.mercadoviviendaapi.commons.exceptions;

import java.util.List;

public class BadRequestException extends RuntimeException {
    private List<String> messages;

    public BadRequestException(List<String> messages) {
        super();
        this.messages = messages;
    }

    public BadRequestException(String message) {
        super(message);
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

}
