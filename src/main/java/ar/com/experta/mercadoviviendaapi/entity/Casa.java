package ar.com.experta.mercadoviviendaapi.entity;

public class Casa {
    private String compra;
    private String venta;
    private String agencia;
    private String nombre;
    private String decimales;
    private String mejorCompra;
    private String mejorVenta;
    private String fecha;

    public String getCompra() {
        return compra;
    }

    public void setCompra(String compra) {
        this.compra = compra;
    }

    public String getVenta() {
        return venta;
    }

    public void setVenta(String venta) {
        this.venta = venta;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDecimales() {
        return decimales;
    }

    public void setDecimales(String decimales) {
        this.decimales = decimales;
    }

    public String getMejorCompra() {
        return mejorCompra;
    }

    public void setMejorCompra(String mejorCompra) {
        this.mejorCompra = mejorCompra;
    }

    public String getMejorVenta() {
        return mejorVenta;
    }

    public void setMejorVenta(String mejorVenta) {
        this.mejorVenta = mejorVenta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Casa(String compra, String venta, String agencia, String nombre, String decimales, String mejorCompra, String mejorVenta, String fecha) {
        this.compra = compra;
        this.venta = venta;
        this.agencia = agencia;
        this.nombre = nombre;
        this.decimales = decimales;
        this.mejorCompra = mejorCompra;
        this.mejorVenta = mejorVenta;
        this.fecha = fecha;
    }
}
