package ar.com.experta.mercadoviviendaapi.entity;

import java.io.Serializable;

public class ResponseDataArray implements Serializable {
    private String idLocalizacion;
    private String cp;
    private String localidad;
    private String provincia;
    private String region;
    private String departamentoPbDesde;
    private String departamentoPbHasta;
    private String departamentoPaDesde;
    private String departamentoPaHasta;
    private String casaDesde;
    private String casaHasta;
    private String CountryDesde;
    private String CountryHasta;
    private String localCalleDesde;
    private String localcalleHasta;
    private String localGaleriaDesde;
    private String localGaleriaHasta;
    private String localEdificioDesde;
    private String localEdifcioHasta;

    public ResponseDataArray() {
    }

    public ResponseDataArray(String idLocalizacion, String cp, String localidad, String provincia, String region, String departamentoPbDesde, String departamentoPbHasta, String departamentoPaDesde, String departamentoPaHasta, String casaDesde, String casaHasta, String countryDesde, String countryHasta, String localCalleDesde, String localcalleHasta, String localGaleriaDesde, String localGaleriaHasta, String localEdificioDesde, String localEdifcioHasta) {
        this.idLocalizacion = idLocalizacion;
        this.cp = cp;
        this.localidad = localidad;
        this.provincia = provincia;
        this.region = region;
        this.departamentoPbDesde = departamentoPbDesde;
        this.departamentoPbHasta = departamentoPbHasta;
        this.departamentoPaDesde = departamentoPaDesde;
        this.departamentoPaHasta = departamentoPaHasta;
        this.casaDesde = casaDesde;
        this.casaHasta = casaHasta;
        CountryDesde = countryDesde;
        CountryHasta = countryHasta;
        this.localCalleDesde = localCalleDesde;
        this.localcalleHasta = localcalleHasta;
        this.localGaleriaDesde = localGaleriaDesde;
        this.localGaleriaHasta = localGaleriaHasta;
        this.localEdificioDesde = localEdificioDesde;
        this.localEdifcioHasta = localEdifcioHasta;
    }

    public String getIdLocalizacion() {
        return idLocalizacion;
    }

    public void setIdLocalizacion(String idLocalizacion) {
        this.idLocalizacion = idLocalizacion;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDepartamentoPbDesde() {
        return departamentoPbDesde;
    }

    public void setDepartamentoPbDesde(String departamentoPbDesde) {
        this.departamentoPbDesde = departamentoPbDesde;
    }

    public String getDepartamentoPbHasta() {
        return departamentoPbHasta;
    }

    public void setDepartamentoPbHasta(String departamentoPbHasta) {
        this.departamentoPbHasta = departamentoPbHasta;
    }

    public String getDepartamentoPaDesde() {
        return departamentoPaDesde;
    }

    public void setDepartamentoPaDesde(String departamentoPaDesde) {
        this.departamentoPaDesde = departamentoPaDesde;
    }

    public String getDepartamentoPaHasta() {
        return departamentoPaHasta;
    }

    public void setDepartamentoPaHasta(String departamentoPaHasta) {
        this.departamentoPaHasta = departamentoPaHasta;
    }

    public String getCasaDesde() {
        return casaDesde;
    }

    public void setCasaDesde(String casaDesde) {
        this.casaDesde = casaDesde;
    }

    public String getCasaHasta() {
        return casaHasta;
    }

    public void setCasaHasta(String casaHasta) {
        this.casaHasta = casaHasta;
    }

    public String getCountryDesde() {
        return CountryDesde;
    }

    public void setCountryDesde(String countryDesde) {
        CountryDesde = countryDesde;
    }

    public String getCountryHasta() {
        return CountryHasta;
    }

    public void setCountryHasta(String countryHasta) {
        CountryHasta = countryHasta;
    }

    public String getLocalCalleDesde() {
        return localCalleDesde;
    }

    public void setLocalCalleDesde(String localCalleDesde) {
        this.localCalleDesde = localCalleDesde;
    }

    public String getLocalcalleHasta() {
        return localcalleHasta;
    }

    public void setLocalcalleHasta(String localcalleHasta) {
        this.localcalleHasta = localcalleHasta;
    }

    public String getLocalGaleriaDesde() {
        return localGaleriaDesde;
    }

    public void setLocalGaleriaDesde(String localGaleriaDesde) {
        this.localGaleriaDesde = localGaleriaDesde;
    }

    public String getLocalGaleriaHasta() {
        return localGaleriaHasta;
    }

    public void setLocalGaleriaHasta(String localGaleriaHasta) {
        this.localGaleriaHasta = localGaleriaHasta;
    }

    public String getLocalEdificioDesde() {
        return localEdificioDesde;
    }

    public void setLocalEdificioDesde(String localEdificioDesde) {
        this.localEdificioDesde = localEdificioDesde;
    }

    public String getLocalEdifcioHasta() {
        return localEdifcioHasta;
    }

    public void setLocalEdifcioHasta(String localEdifcioHasta) {
        this.localEdifcioHasta = localEdifcioHasta;
    }
}
