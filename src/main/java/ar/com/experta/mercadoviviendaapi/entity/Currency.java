package ar.com.experta.mercadoviviendaapi.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class Currency {
    @SerializedName("casa")
    private Casa casa;

    public Currency() {
    }

    public Currency(Casa casa, Map<String, Object> additionalProperties) {
        this.casa = casa;
    }

    public Casa getCasa() {
        return casa;
    }

    public void setCasa(Casa casa) {
        this.casa = casa;
    }


}
