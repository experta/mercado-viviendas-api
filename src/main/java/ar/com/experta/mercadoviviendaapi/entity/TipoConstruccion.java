package ar.com.experta.mercadoviviendaapi.entity;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum TipoConstruccion {
    CASA,
    COUNTRY,
    EDIFICIO_PB,
    EDIFICIO_PA,
    LOCAL_CALLE,
    LOCAL_GALERIA,
    LOCAL_EDIFICIO;
}