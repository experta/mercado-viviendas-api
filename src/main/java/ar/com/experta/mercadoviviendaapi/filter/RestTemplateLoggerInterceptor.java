package ar.com.experta.mercadoviviendaapi.filter;

import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class RestTemplateLoggerInterceptor
        implements ClientHttpRequestInterceptor {

    private static final Logger logger = LoggerFactory.getLogger(RestTemplateLoggerInterceptor.class);

    @Override
    public ClientHttpResponse intercept(
            HttpRequest request,
            byte[] body,
            ClientHttpRequestExecution execution) throws IOException {

        //logueo el metodo y el body
        logger.info(String.format("Ejecutando Http Request: METHOD=%s , URI=%s", request.getMethod().toString(),
                request.getURI()));

        String stringifiedBody = new String(body, "UTF-8");
        if (StringUtils.isNotEmpty(stringifiedBody))
            this.logger.info("Request body: {}", stringifiedBody);

        ClientHttpResponse response = execution.execute(request, body);

        //logueamos la respuesta si el response tiene agun error
        if (response.getStatusCode().value() >= HttpStatus.BAD_REQUEST.value()) {
            this.logger.error("Error haciendo Http Request a {}. StatusCode : {} , StatusText : {}", request.getURI(),
                    response.getStatusCode(), response.getStatusText());
        }

        return response;
    }
}