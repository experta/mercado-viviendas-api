package ar.com.experta.mercadoviviendaapi.controller;

import ar.com.experta.mercadoviviendaapi.entity.ResponseDataArray;
import ar.com.experta.mercadoviviendaapi.entity.TipoConstruccion;
import ar.com.experta.mercadoviviendaapi.service.SearchValue;
import ar.com.experta.mercadoviviendaapi.service.api.IDivisaService;
import ar.com.experta.mercadoviviendaapi.service.api.IDtoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;


@Controller
@Validated
@RestController
@ResponseStatus(HttpStatus.BAD_REQUEST)
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/negocio")
@Api(tags="Negocio", description="Endpoints con información de objetos de negocio")
public class NegocioController {

    @Autowired
    private IDtoService idtoService;
    @Autowired
    private IDivisaService iDivisaService;

    @Autowired
    private SearchValue searchValue;

    @GetMapping(value = "/valorConstruccion/{IdLocalizacion}/{tipoVivienda}")
    @ApiOperation(value = "Valor de Vivienda")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK"),
            @ApiResponse(code = 400, message = "Errónea petición"),
            @ApiResponse(code = 502, message = "Servicio no disponible")})
    public ResponseEntity <HashMap>  getValorConstruccionMetroCuadrado( @PathVariable String IdLocalizacion, @Valid @PathVariable("tipoVivienda") TipoConstruccion tipoConstruccion){

        HashMap<String, ResponseDataArray> dataResponse = idtoService.getAllDataFromValoresConstruccion();
        HashMap<String, String> dataCurrency = iDivisaService.getValor();
        return ResponseEntity.ok(searchValue.getValorConstruccion(dataResponse,IdLocalizacion, tipoConstruccion,dataCurrency));
    }


    @GetMapping(value = "/valor_dolar")
    @ApiOperation(value = "Valor de Vivienda")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operación OK"),
            @ApiResponse(code = 400, message = "Errónea petición"),
            @ApiResponse(code = 502, message = "Servicio no disponible")})
    public ResponseEntity <HashMap>  getCotizacionDolar( ){

       
        HashMap<String, String> dataCurrency = iDivisaService.getValor();
        return ResponseEntity.ok(dataCurrency);
    }
}
