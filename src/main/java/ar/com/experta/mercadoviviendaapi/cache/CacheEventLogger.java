package ar.com.experta.mercadoviviendaapi.cache;

import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CacheEventLogger implements CacheEventListener<Object, Object> {

    private static final Logger logger = LoggerFactory.getLogger(CacheEventLogger.class);
    @SuppressWarnings("unchecked")
    @Override
    public void onEvent(CacheEvent<? extends Object, ? extends Object> cacheEvent) {
//        logger.info("Updating the cache with oldvalue={}, newValue={}", cacheEvent.getOldValue(),
//                cacheEvent.getNewValue());
    }
}