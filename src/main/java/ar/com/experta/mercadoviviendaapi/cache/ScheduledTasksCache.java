package ar.com.experta.mercadoviviendaapi.cache;

import ar.com.experta.mercadoviviendaapi.entity.ResponseDataArray;
import ar.com.experta.mercadoviviendaapi.service.api.IDivisaService;
import ar.com.experta.mercadoviviendaapi.service.api.IDtoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class ScheduledTasksCache {

    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasksCache.class);
    @Autowired
    private CacheManager cacheManager;
    @Autowired
    private IDtoService iDtoService;
    private IDivisaService iDivisaService;

    @Value("${CACHE_AWAIT_TIMER_ONERROR}")
    private Integer sleep;

    @Scheduled(cron = "0 0 8 * * *")
    public void task() {

        logger.info("Cron Task update cache: task started ");
        try {

                final Cache cache = cacheManager.getCache("ConstruccionCache");
                if (cache!=null){
                    SimpleValueWrapper valWrap = (SimpleValueWrapper) cache.get(SimpleKey.EMPTY);
                    cache.clear();
                    logger.info("Cache deleted.");

                    HashMap<String, ResponseDataArray> dataCached = null;
                    dataCached = populateAllData();
                    if (dataCached == null) {
                        if (valWrap == null) {
                            logger.info("Old cache and new cache is null, starting the recursive method to populate the cache with data");
                            populateCache();
                        } else {
                            logger.info("Restoring the cache with previousData");
                            cache.put(SimpleKey.EMPTY, valWrap.get());
                        }
                    }
                    logger.info("Cron Task update cache: Success.");
                }
        } catch (Exception e) {
                logger.error("Cron Task update cache: ERROR.", e);
        } finally {
            final Cache cache = cacheManager.getCache("ConstruccionValorCache");
            if (cache!=null) {
                cache.clear();
            }
            logger.info("Cache deleted.");
            try {
                logger.info("executing method with cache: getAllData");
                iDivisaService.getValor();
            } catch (Exception e) {
                logger.error("Method return error: ", e);
            }

        }
    }

    private HashMap<String, ResponseDataArray> populateAllData (){
            logger.info("executing method with cache: getAllData");
            return  iDtoService.getAllDataFromValoresConstruccion();
    }

    private void populateCache() {
        boolean cacheIsNull = true;
        HashMap<String, ResponseDataArray> dataCached = null;
        while (cacheIsNull) {
            try {
                logger.info("Trying to populate the cache...");
                dataCached = iDtoService.getAllDataFromValoresConstruccion();
            } catch (Exception e) {
                logger.error("Error populating the cache: ", e);
            }
            if (dataCached == null) {
                try {
                    logger.info("Waiting to re-execute the method.");
                    Thread.sleep(sleep);                // 30 seconds
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    logger.error("Error trying to sleep the thread, will be interrupted: ",
                            e);
                }
            } else {
                cacheIsNull = false;
            }
        }
    }
}

