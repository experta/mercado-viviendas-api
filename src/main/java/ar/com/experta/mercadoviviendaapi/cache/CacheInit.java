package ar.com.experta.mercadoviviendaapi.cache;

import ar.com.experta.mercadoviviendaapi.service.api.IDivisaService;
import ar.com.experta.mercadoviviendaapi.service.api.IDtoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

@Service
public class CacheInit implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger logger = LoggerFactory.getLogger(CacheInit.class);

    @Autowired
    IDtoService iDtoService;
    @Autowired
    IDivisaService iDivisaService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {

        try {
            logger.info("App started, trying to init the cache Data...");
            iDtoService.getAllDataFromValoresConstruccion();
            logger.info("App started, trying to init the cache Divisa...");
            iDivisaService.getValor();
        } catch (Exception e) {
            logger.error("error trying to init the data of the cache", e);
        }
    }

}