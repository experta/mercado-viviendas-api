package ar.com.experta.mercadoviviendaapi.service.api;

import ar.com.experta.mercadoviviendaapi.entity.ResponseDataArray;

import java.util.HashMap;

public interface IDtoService {
    HashMap<String, ResponseDataArray> getAllDataFromValoresConstruccion();

}
