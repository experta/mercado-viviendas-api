package ar.com.experta.mercadoviviendaapi.service;

import ar.com.experta.mercadoviviendaapi.entity.ResponseDataArray;
import ar.com.experta.mercadoviviendaapi.entity.TipoConstruccion;
import ar.com.experta.mercadoviviendaapi.service.api.IDtoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.DecimalFormat;
import java.util.HashMap;

@Service
public class SearchValue {
    private static final Logger logger = LoggerFactory.getLogger(SearchValue.class);
    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private IDtoService idtoService;

    public HashMap<String,String> getValorConstruccion(HashMap<String, ResponseDataArray> rda, String IdLocalizacion, TipoConstruccion tipoConstruccion, HashMap<String, String>  currencylist ) {
        String sValor_D = " El id de localizacion no es correcto";
        String sValor_H = " El id de localizacion no es correcto";

        int valorActual = 1;
        ResponseDataArray obteinRecord = new ResponseDataArray();
        if (rda.containsKey(IdLocalizacion)) {
            obteinRecord = rda.get(IdLocalizacion);
            switch (tipoConstruccion) {
                case  EDIFICIO_PB:
                    sValor_D = obteinRecord.getDepartamentoPbDesde();
                    sValor_H = obteinRecord.getDepartamentoPaHasta();
                    break;
                case EDIFICIO_PA:
                    sValor_D = obteinRecord.getDepartamentoPaDesde();
                    sValor_H = obteinRecord.getDepartamentoPaHasta();
                    break;
                case CASA:
                    sValor_D = obteinRecord.getCasaDesde();
                    sValor_H = obteinRecord.getCasaHasta();
                    break;
                case COUNTRY:
                    sValor_D = obteinRecord.getCountryDesde();
                    sValor_H = obteinRecord.getCountryHasta();
                    break;
                case LOCAL_CALLE:
                    sValor_D = obteinRecord.getLocalCalleDesde();
                    sValor_H = obteinRecord.getLocalcalleHasta();
                    break;
                case LOCAL_GALERIA:
                    sValor_D = obteinRecord.getLocalGaleriaDesde();
                    sValor_H = obteinRecord.getLocalGaleriaHasta();
                    break;
                case LOCAL_EDIFICIO:
                    sValor_D = obteinRecord.getLocalEdificioDesde();
                    sValor_H = obteinRecord.getLocalEdifcioHasta();
                    break;
                default:
                    sValor_D = "Tipo de vivienda desconocida";
                    sValor_H = "Tipo de vivienda desconocida";
                    break;
            }
        }
        DecimalFormat df = new DecimalFormat("###.00");
        double v = 0;
        if(currencylist.containsKey("Dolar Oficial") ){
            sValor_D = df.format(Double.parseDouble(currencylist.get("Dolar Oficial").toString().replace(",", "."))* Integer.valueOf(sValor_D.replace(".","")) ) ;
            sValor_H = df.format(Double.parseDouble(currencylist.get("Dolar Oficial").toString().replace(",", "."))* Integer.valueOf(sValor_H.replace(".","")) ) ;

        }
        HashMap   respuesta = new HashMap();
        respuesta.put("valorDesde ",sValor_D);
        respuesta.put("valorHasta",sValor_H);
        return respuesta;
    }

}
