package ar.com.experta.mercadoviviendaapi.service.impl;

import ar.com.experta.mercadoviviendaapi.commons.ConnectionFailException;
import ar.com.experta.mercadoviviendaapi.constant.Constant;
import ar.com.experta.mercadoviviendaapi.entity.ResponseDataArray;
import ar.com.experta.mercadoviviendaapi.service.api.IDtoService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.util.HashMap;

@Service
public class NegocioService implements IDtoService {
    private static final Logger logger =  LoggerFactory.getLogger(NegocioService.class);
    private ResourceLoader resourceLoader;
    private RestTemplate restTemplate = new RestTemplate();

    @Override
    @Cacheable(value="ConstruccionCache")
    public HashMap<String, ResponseDataArray> getAllDataFromValoresConstruccion() {
        HashMap <String, ResponseDataArray> parsedResponse = new HashMap<>();
        String rutaArchivoLocal;
        final String CHARSET = "UTF-8";
        long initTimeer=System.currentTimeMillis();
        logger.info("getAll:: asking for information");
        try {
            InputStream inputStream = getClass().getResourceAsStream("/static/CSVApiPreciosMercados.csv");
            Reader in = new BufferedReader(new InputStreamReader(inputStream, CHARSET));
            logger.info("getAll:: After create reader");
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(in);
            logger.info("getAll:: After Iterable CSVRecords");
            for (CSVRecord record : records) {
                if (!record.get(0).equalsIgnoreCase(Constant.ID_LOCALIZACION)) {
                    ResponseDataArray responseDataArray = new ResponseDataArray();
                    responseDataArray.setIdLocalizacion(record.get(0));
                    responseDataArray.setCp(record.get(1));
                    responseDataArray.setLocalidad(record.get(2));
                    responseDataArray.setProvincia(record.get(3));
                    responseDataArray.setRegion (record.get(4));
                    responseDataArray.setDepartamentoPaDesde(record.get(5));
                    responseDataArray.setDepartamentoPaHasta(record.get(6));
                    responseDataArray.setDepartamentoPbDesde(record.get(7));
                    responseDataArray.setDepartamentoPbHasta(record.get(8));
                    responseDataArray.setCasaDesde(record.get(9));
                    responseDataArray.setCasaHasta(record.get(10));
                    responseDataArray.setCountryDesde(record.get(11));
                    responseDataArray.setCountryHasta(record.get(12));
                    //nuevas columnas
                    responseDataArray.setLocalCalleDesde(record.get(13));
                    responseDataArray.setLocalcalleHasta(record.get(14));
                    responseDataArray.setLocalGaleriaDesde(record.get(15));
                    responseDataArray.setLocalGaleriaHasta(record.get(16));
                    responseDataArray.setLocalEdifcioHasta(record.get(17));
                    responseDataArray.setLocalEdificioDesde(record.get(18));
                    parsedResponse.put(record.get(0), responseDataArray);
                }

            }
            logger.info("getAll:: END ");

        } catch (FileNotFoundException e) {
            logger.info("getAllPlan ::".concat(e.getMessage()).concat(e.getCause().toString()));
            throw  new ConnectionFailException("getAll :: catch error ") ;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return parsedResponse;
    }


}
