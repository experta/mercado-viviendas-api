package ar.com.experta.mercadoviviendaapi.service.impl;

import ar.com.experta.mercadoviviendaapi.service.api.IDivisaService;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DivisaService implements IDivisaService {
    private static final Logger logger = LoggerFactory.getLogger(NegocioService.class);
    //@Value("${URLDOLAR}")
    private String uri="https://api.estadisticasbcra.com/usd_of";
    private ResourceLoader resourceLoader;
    private RestTemplate restTemplate = new RestTemplate();

    @Override
    @Cacheable(value="ConstruccionValorCache")
    public HashMap<String, String> getValor(){

        String name = "Authorization";
        String value = "Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjY5MjU4OTUsInR5cGUiOiJleHRlcm5hbCIsInVzZXIiOiJqYXZpZXIyNWFyZ0BnbWFpbC5jb20ifQ.KdZ6sUzG-_1A4riAAAQEy5g2tyVyVsOasJYwu6jlrYP9svpXRkxqHu_1kxfwr58CokCTfKKsMVJxl-3ISj7oag";

        HttpHeaders headers = new HttpHeaders();
        headers.add(name, value);

        try {
            RequestEntity<Void> requestEntity = RequestEntity.get(new URI(uri)).headers(headers).build();
            ResponseEntity<String> response = restTemplate.exchange(requestEntity, String.class);

            String body = response.getBody();

            List<Object> objectList = new Gson().fromJson(body, new TypeToken<List<Object>>() {}.getType());
            ArrayList<Object> arrayList = new ArrayList<>(objectList);

            int longitud = arrayList.size();
            Object ultimaCotizacion = arrayList.get(longitud - 1);

            String json = new Gson().toJson(ultimaCotizacion);
            Map<String, String> map = new Gson().fromJson(json, new TypeToken<Map<String, String>>() {}.getType());

            HashMap<String, String> monedas = new HashMap();

            monedas.put("Fecha", map.get("d"));
            monedas.put("Dolar Oficial", map.get("v"));


            return monedas;

        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
    /*@Override
    @Cacheable(value = "ConstruccionValorCache")
    public HashMap<String, String> getValor() {
        String result = restTemplate.getForObject(uri, String.class);
        DecimalFormat df = new DecimalFormat("###.00");
        Gson gson = new Gson();
        HashMap<String, String> monedas = new HashMap<>();

        try {
            JsonArray jsonArray = gson.fromJson(result, JsonArray.class);

            for (JsonElement jsonElement : jsonArray) {
                JsonObject casaObject = jsonElement.getAsJsonObject().getAsJsonObject("casa");
                JsonElement nombreElement = casaObject.get("nombre");
                JsonElement ventaElement = casaObject.get("venta");

                if (nombreElement != null && nombreElement.isJsonPrimitive() && ventaElement != null && ventaElement.isJsonPrimitive()) {
                    String nombre = nombreElement.getAsString();
                    String venta = ventaElement.getAsString();
                    monedas.put(nombre, venta);
                } else {
                    // Manejar caso inesperado de JSON
                    logger.error("Formato JSON inesperado: {}", jsonElement.toString());
                }
            }
        } catch (Exception e) {
            logger.error("Error al procesar JSON: {}", e.getMessage(), e);
        }
        return monedas;
    }*/
}
