package ar.com.experta.mercadoviviendaapi.service.api;

import java.util.HashMap;

public interface IDivisaService {
    HashMap<String, String>  getValor();
}
