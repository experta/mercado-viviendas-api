package ar.com.experta.mercadoviviendaapi.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
@EnableCaching
public class AppConfig {
    @Value("${TTLRESTTEMPLATE}")
    private long ttlrestTemplate;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder){

        return builder.setConnectTimeout(Duration.ofMillis(ttlrestTemplate))
                .setReadTimeout(Duration.ofMillis(ttlrestTemplate)).build();
    }

}
