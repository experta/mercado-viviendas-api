package ar.com.experta.mercadoviviendaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableCaching
@EnableAsync
@EnableScheduling
public class MercadoViviendaApiApplication {

	public static void main(String[] args) {
		System.out.println("Mercado vivienda");
		SpringApplication.run(MercadoViviendaApiApplication.class, args);
	}

}
